# docker-compose systemd service

A systemd service to reliably integrate a set of containers defined in a docker-compose into your system.

## Setup

- clone the repo somewhere (~/docker/systemd-service in the example) with `git clone git@gitlab.com:twinter/docker-compose-systemd-service.git ~/docker/systemd-service`
- create a symlink to the service file in /etc/systemd/sytem with `sudo ln -s /absolute/path/to/systemd-service/docker-compose@.service /etc/systemd/system/docker-compose@.service`

## Usage

- place (or symlink) your docker-compose projects in a folder in /etc/docker/compose (called "own-project" in the example) so you have a docker-compose file at /etc/docker/compose/own-project/docker-compose.yml with `sudo ln -s /home/<user>/docker/<service> /etc/docker/compose/<service>` (replace `<user>` and `<service>`, adapt source path to your system)
- enable and start the systemd service for the project by running `sudo systemctl enable --now docker-compose@own-project`

You can do all of the usual systemd stuff (start/stop/reload/restart/...) with it under this name.
